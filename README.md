# README #

Here you can download the datasets and R codes used in the Tripathi et al. (2018) 

### What is this repository for? ###

* This repository contains datasets of tree and ground vegetation, bird incidence and their functional traits in a .Rdata file   
* It also consist of R codes used to analysed the above datasets
* The datasets are analysed in bayesian framework using JAGS. The repository contains the JAGS software that should be installed before analysing the datsets

### How do I get set up? ###

* Download data and R codes from the source section in one folder
* Install JAGS on your ssystem
* Load R codes in your R software / R studio
* Follow the instructions in the R code file 

### Who do I talk to? ###

* For any questions and/or feedback contact hemanttripathi05@gmail.com or casey.ryan@ed.ac.uk 